<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link        
 * @since      1.0.0
 *
 * @package    Delivery_Moment
 * @subpackage Delivery_Moment/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Delivery_Moment
 * @subpackage Delivery_Moment/includes
 * @author       < >
 */
class Delivery_Moment {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Delivery_Moment_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

    /**
     * Name of meta field that is used for setting up delivery time
     * @var string
     */
	public static $meta_key = 'spec_one';

    /**
     * Value of 'spec_one' meta field
     * @var string
     */
	public static $meta_val = 'Voor 16.00 besteld is morgen in huis';

    /**
     * Order meta name that is used for storing delivery moment
     * @var string
     */
    public static $order_meta_key = 'al_delivery_moment';

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'DELIVERY_MOMENT_VERSION' ) ) {
			$this->version = DELIVERY_MOMENT_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'delivery-moment';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Delivery_Moment_Loader. Orchestrates the hooks of the plugin.
	 * - Delivery_Moment_i18n. Defines internationalization functionality.
	 * - Delivery_Moment_Admin. Defines all hooks for the admin area.
	 * - Delivery_Moment_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-delivery-moment-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-delivery-moment-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-delivery-moment-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-delivery-moment-public.php';

		$this->loader = new Delivery_Moment_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Delivery_Moment_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Delivery_Moment_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Delivery_Moment_Admin( $this->get_plugin_name(), $this->get_version() );
        $this->loader->add_action('woocommerce_admin_order_data_after_shipping_address', $plugin_admin, 'print_order_delivery_moment');

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Delivery_Moment_Public( $this->get_plugin_name(), $this->get_version() );

        // adds field to checkout page
        $this->loader->add_action('woocommerce_after_order_notes', $plugin_public, 'checkout_page_fields');
        // validation - delivery moment is required
        $this->loader->add_action('woocommerce_checkout_process', $plugin_public, 'woocommerce_checkout_process');
        // update order meta
        $this->loader->add_action('woocommerce_checkout_update_order_meta', $plugin_public, 'woocommerce_checkout_update_order_meta', 10, 1);
        // prints delivery moment on email templates
        $this->loader->add_action('woocommerce_email_after_order_table', $plugin_public, 'email_order_delivery_moment');
        // prints delivery moment on order details table in my account and thank you pages
        $this->loader->add_action('woocommerce_order_details_after_order_table_items', $plugin_public, 'order_details_delivery_moment', 100);
        // debug
        //$this->loader->add_action( 'wp_head', $plugin_public, 'debug');
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Delivery_Moment_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}
}
