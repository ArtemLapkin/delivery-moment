<?php

/**
 * Fired during plugin deactivation
 *
 * @link        
 * @since      1.0.0
 *
 * @package    Delivery_Moment
 * @subpackage Delivery_Moment/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Delivery_Moment
 * @subpackage Delivery_Moment/includes
 * @author       < >
 */
class Delivery_Moment_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
