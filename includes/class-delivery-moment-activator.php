<?php

/**
 * Fired during plugin activation
 *
 * @link        
 * @since      1.0.0
 *
 * @package    Delivery_Moment
 * @subpackage Delivery_Moment/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Delivery_Moment
 * @subpackage Delivery_Moment/includes
 * @author       < >
 */
class Delivery_Moment_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
