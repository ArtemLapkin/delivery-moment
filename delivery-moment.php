<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link               
 * @since             1.0.0
 * @package           Delivery_Moment
 *
 * @wordpress-plugin
 * Plugin Name:       Delivery Moment
 * Plugin URI:         
 * Description:       Product delivery options depending on meta_key called "spec_one".
 * Version:           1.0.0
 * Author:             
 * Author URI:         
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       delivery-moment
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'DELIVERY_MOMENT_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-delivery-moment-activator.php
 */
function activate_delivery_moment() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-delivery-moment-activator.php';
	Delivery_Moment_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-delivery-moment-deactivator.php
 */
function deactivate_delivery_moment() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-delivery-moment-deactivator.php';
	Delivery_Moment_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_delivery_moment' );
register_deactivation_hook( __FILE__, 'deactivate_delivery_moment' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-delivery-moment.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_delivery_moment() {

	$plugin = new Delivery_Moment();
	$plugin->run();

}
run_delivery_moment();
