<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link        
 * @since      1.0.0
 *
 * @package    Delivery_Moment
 * @subpackage Delivery_Moment/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Delivery_Moment
 * @subpackage Delivery_Moment/admin
 * @author       < >
 */
class Delivery_Moment_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

    public function print_order_delivery_moment($order) {
	    $meta = get_post_meta($order->get_ID(), Delivery_Moment::$order_meta_key, true);
	    if( $meta !== '' ) {
	        echo '<h3>' . __('Delivery moment', 'delivery-moment') . '</h3>';
	        echo '<p>' . $meta . '</p>';
        }
	}
}
