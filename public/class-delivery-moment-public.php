<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link        
 * @since      1.0.0
 *
 * @package    Delivery_Moment
 * @subpackage Delivery_Moment/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Delivery_Moment
 * @subpackage Delivery_Moment/public
 * @author       < >
 */
class Delivery_Moment_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}


    /**
     * Get product delivery moment meta
     * @param bool $product_id
     * @return string
     */
    public function get_product_spec_one($product_id = false) {

        if( $product_id === false ) {
            if( function_exists( is_product() ) && is_product() ) {
                $product_id = get_the_ID();
            }
        }

        if( $product_id === false ) {
            return '';
        }

        //$meta = get_post_meta($product_id, Delivery_Moment::$meta_key, true);

        // debug
        $meta = '';
        if( in_array( $product_id, array( 10, 18, 27 ) )  ) {
            $meta = Delivery_Moment::$meta_val;
        }
        // debug end

        return $meta;
    }

    /**
     * Temporary function
     */
    public function debug() {
        var_dump($this->get_product_spec_one());
    }

    /**
     * Prints radio buttons on checkout page with delivery moment
     * @param $checkout
     */
    public function checkout_page_fields($checkout) {
	    // get available delivery moment depending on products in cart
        $delivery_options = $this->get_delivery_options_depending_on_cart();

        if( empty($delivery_options) ) {
            return;
        }


        $value_in_session = $checkout->get_value('al_delivery_moment');

        // first delivery moment is available by default
        reset($delivery_options);
        $first_value = key($delivery_options);
        $selected = $value_in_session !== null ? $value_in_session :  $first_value;

        $args = array(
            'type' => 'radio',
            'class' => array( 'form-row-wide' ),
            'options' => $delivery_options,
            'default' => $selected,
            'required' => true
        );

        echo '<div id="al-delivery-moment">';
        echo '<h3>' . __('Please select delivery moment', 'delivery-moment') . '</h3>';
        echo '<style>#al_delivery_moment_field input{width:20px;display:inline-block;}#al_delivery_moment_field label{width:calc(100% - 20px);margin:0;display:inline-block;}</style>';
        woocommerce_form_field( 'al_delivery_moment', $args );
        echo '</div>';
    }

    /**
     * Returns delivery moment options, depending on products in the cart
     * @return array
     */
    private function get_delivery_options_depending_on_cart() {
        global $woocommerce;
        $items = $woocommerce->cart->get_cart();
        if( empty( $items ) ) {
            return array();
        }

        // if any of the product doesn't contain needed meta data - do not show radio buttons
        foreach ( $items as $item => $values ) {
            if( $this->get_product_spec_one($values['data']->get_id()) !== Delivery_Moment::$meta_val ) {
                return array();
            }
        }

        $out = $this->get_delivery_options_depending_on_time();

        return $out;
    }

    /**
     * Returns delivery time options depending on current day/time
     * @return array
     */
    private function get_delivery_options_depending_on_time() {
	    $timestamp = time() + $this->correctional_factor();

	    $current_week_day = date('D', $timestamp);
        $current_hour = date('H', $timestamp);

        $out = array();

	    switch ($current_week_day) {
            case 'Sun':
            case 'Mon':
            case 'Tue':
            case 'Wed':
            case 'Thu':
                $day = $this->get_delivery_option(1, 'night');
                $out = array();
                if( ! empty( $day ) ) {
                    $out[$day['value']] = $day['label'];
                }

                $day = $this->get_delivery_option(2, 'day');
                if( ! empty( $day ) ) {
                    $out[$day['value']] = $day['label'];
                }
                break;
            case 'Fri':
                // Friday before 16:00
                if( $current_hour < 16 ) {
                    $day = $this->get_delivery_option(1, 'day');
                    if( ! empty( $day ) ) {
                        $out[$day['value']] = $day['label'];
                    }
                } else {
                    // Friday after 16:00
                    $day = $this->get_delivery_option(3, 'night');
                    if( ! empty( $day ) ) {
                        $out[$day['value']] = $day['label'];
                    }
                }
                break;
            case 'Sat':
                // Saturday before 16:00
                if( $current_hour < 16 ) {
                    $day = $this->get_delivery_option(2, 'night');
                    if( ! empty( $day ) ) {
                        $out[$day['value']] = $day['label'];
                    }
                } else {
                    // Saturday after 16:00
                    $day = $this->get_delivery_option(2, 'night');
                    if( ! empty( $day ) ) {
                        $out[$day['value']] = $day['label'];
                    }
                    $day = $this->get_delivery_option(3, 'day');
                    if( ! empty( $day ) ) {
                        $out[$day['value']] = $day['label'];
                    }
                }
                break;
        }
        return $out;
    }

    /**
     * Helper that returns delivery moment for particular condition
     * @param $how_far_next_day
     * @param $delivery_time
     * @return array
     */
    private function get_delivery_option($how_far_next_day, $delivery_time) {

	    if( ! in_array($delivery_time, array( 'night', 'day' )) ) {
	        return array();
        }

	    $delivery_moment = $delivery_time === 'night' ? '18.00 - 22.00' : '9.00 - 18.00';

	    $next_timestamp = strtotime('+' . $how_far_next_day . ' day') + $this->correctional_factor();

        $next_date_val = date('Y-m-d', $next_timestamp);
        $next_date_label = date('d M', $next_timestamp);

        $out = array (
            'status' => '',
            'value' => $next_date_val . ', ' . $delivery_moment,
            'label' => $next_date_label . ', ' . $delivery_moment
        );

        return $out;
    }

    /**
     * Validates if delivery moment radiobutton exists but wasn't selected
     */
    public function woocommerce_checkout_process() {
	    if( ! empty( $this->get_delivery_options_depending_on_cart() ) && ! $_POST['al_delivery_moment'] ) {
            wc_add_notice( __( 'Please select delivery moment', 'delivery-moment' ), 'error' );
        }
    }

    /**
     * Adds note with delivery moment
     * @param $order_id
     */
    public function woocommerce_checkout_update_order_meta($order_id) {
	    if( isset( $_POST['al_delivery_moment'] ) ) {
	        // updates order 'note'
            $order = wc_get_order(  $order_id );
            $note = __('Delivery moment - ') . trim( $_POST['al_delivery_moment'] );
            $order->add_order_note( $note );

            update_post_meta($order_id, Delivery_Moment::$order_meta_key, trim( $_POST['al_delivery_moment'] ));
        }
    }

    /**
     * Prints delivery moment on email template
     * @param $order
     */
    public function email_order_delivery_moment($order) {
        $meta = $this->get_order_meta($order);
        if( $meta !== '' ) {
            echo '<div style="margin-bottom:40px">';
            echo '<h2>'.__( 'Delivery Moment:', 'woocommerce' ).'</h2>';
            echo '<p>' . $meta . '</p>';
            echo '</div>';
        }

    }

    /**
     * Prints delivery moment on order details page on front end
     * @param $order
     */
    public function order_details_delivery_moment($order) {
        $meta = $this->get_order_meta($order);
        if( $meta !== '' ) {
            echo '<tr>
                    <th scope="row">' . __('Delivery Moment', 'delivery_moment') . '</th>
                    <td>'.$meta.'</td>
                </tr>';
        }
    }

    /**
     * Returns order delivery moment meta
     * @param $order
     * @return mixed
     */
    private function get_order_meta($order) {
        if( is_object($order) ) {
            $order_id = $order->get_ID();
        } else {
            $order_id = $order;
        }

        return get_post_meta($order_id, Delivery_Moment::$order_meta_key, true);
    }

    /**
     * Helper for correcting time difference between server and user
     * @return int
     */
    private function correctional_factor() {
        // used for debugging
        // $out = 60*60*24*3  + 60*60*8;

        $out = 0;
        return $out;
    }
}
